FROM golang:1.16-alpine AS src

WORKDIR /go/src/app/
ADD . ./

# Build Go Binary
RUN set -ex; \
    CGO_ENABLED=0 GOOS=linux go build -ldflags="-w -s" -o ./calamityservice ./cmd/calamityservice/main.go;

FROM alpine

# Add new user 'appuser'. App should be run without root privileges as a security measure
RUN adduser --home "/home/appuser" --disabled-password appuser --gecos "appuser,-,-,-"
USER appuser
RUN mkdir -p /home/appuser/app
WORKDIR /home/appuser/app/

COPY --from=src /go/src/app/calamityservice .
COPY ./ui ./ui

EXPOSE 3333

# Run Go Binary
CMD /home/appuser/app/calamityservice