async function getOccupantInformation() {
    let adress = document.getElementById("addressField").value
    let url = "http://localhost:3082/api/v1/calamity/?address="+adress;
    try {
        let res = await fetch(url);
        return await res.json();
    } catch (error) {
        console.log(error);
    }
}

async function renderOccupantInformation() {
    let occupantInformation = await getOccupantInformation();
    let html = '';
    let htmlSegment = `<div class="occupantInformation">
                        <span>Aantal bewoners: ${occupantInformation.occupants}</span><br/>
                        <span>Locatie gasfles: ${occupantInformation.location_gas_cannister}</span>
                    </div>`;

    html += htmlSegment;

    let container = document.querySelector('.occupantInformationList');
    container.innerHTML = html;
}
