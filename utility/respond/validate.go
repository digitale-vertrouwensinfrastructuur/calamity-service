// Licensed under the MIT license

package respond

import (
	"fmt"

	"github.com/go-playground/validator/v10"
)

func Validate(v *validator.Validate, generic interface{}) []string {
	err := v.Struct(generic)
	if err == nil {
		return nil
	}

	if _, ok := err.(*validator.InvalidValidationError); ok {
		fmt.Println(err)
		return nil
	}

	var errs []string
	for _, err := range err.(validator.ValidationErrors) {
		errs = append(errs, fmt.Sprintf("%s is %s", err.StructNamespace(), err.Tag()))
	}

	return errs
}
