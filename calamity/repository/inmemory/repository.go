// Licensed under the MIT license

package inmemory

import (
	"context"
	"errors"
	"math/rand"

	"github.com/jmoiron/sqlx"

	calamityModel "gitlab.com/digitale-vertrouwensinfrastructuur/calamity-service/calamity"
)

type repository struct {
	db *sqlx.DB
}

func New(db *sqlx.DB) *repository {
	return &repository{db: db}
}

func (r *repository) Create(ctx context.Context, calamity *calamityModel.Calamity) (*calamityModel.Calamity, error) {
	calamity.CalamityID = int64(rand.Intn(100) + 10)
	calamities = append(calamities, calamity)

	return calamity, nil
}

func (r *repository) All(ctx context.Context, f *calamityModel.Filter) ([]*calamityModel.Calamity, error) {
	return calamities, nil
}

func (r *repository) Read(ctx context.Context, calamityID int64) (*calamityModel.Calamity, error) {
	for _, p := range calamities {
		if p.CalamityID == calamityID {
			return p, nil
		}
	}

	return nil, errors.New("calamity not found")
}

func (r *repository) Update(ctx context.Context, calamity *calamityModel.Calamity) error {
	for i, p := range calamities {
		if p.CalamityID == calamity.CalamityID {
			calamities[i] = calamity
			return nil
		}
	}

	return errors.New("calamity not found")
}

func (r *repository) Delete(ctx context.Context, calamityID int64) error {
	for i, p := range calamities {
		if p.CalamityID == calamityID {
			calamities = append((calamities)[:i], (calamities)[i+1:]...)
			return nil
		}
	}

	return errors.New("calamity not found")
}

func (r *repository) Search(ctx context.Context, f *calamityModel.Filter) ([]*calamityModel.Calamity, error) {
	// TODO: search and return list

	return calamities, nil
}

var calamities = []*calamityModel.Calamity{
	{
		CalamityID: 1,
		Address:    "Duckstad, Kweklaan 36",
		Active:     true,
	},
}
