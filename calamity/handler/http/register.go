// Licensed under the MIT license

package http

import (
	"github.com/go-chi/chi/v5"

	"gitlab.com/digitale-vertrouwensinfrastructuur/calamity-service/calamity"
	"gitlab.com/digitale-vertrouwensinfrastructuur/calamity-service/middleware"
	"gitlab.com/digitale-vertrouwensinfrastructuur/calamity-service/utility/restclient"
)

func RegisterHTTPEndPoints(router *chi.Mux, repo calamity.Repository, client restclient.HTTPClient, policyServiceEndpoint string) {
	h := NewHandler(repo, client, policyServiceEndpoint)

	router.Route("/api/v1/calamity", func(router chi.Router) {
		router.Use(middleware.Json)
		router.Get("/", h.Get)
	})
}
