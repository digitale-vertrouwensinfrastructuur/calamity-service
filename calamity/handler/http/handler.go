// Licensed under the MIT license

package http

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/go-playground/validator/v10"

	"gitlab.com/digitale-vertrouwensinfrastructuur/calamity-service/calamity"
	"gitlab.com/digitale-vertrouwensinfrastructuur/calamity-service/policy"
	"gitlab.com/digitale-vertrouwensinfrastructuur/calamity-service/utility/respond"
	restclient "gitlab.com/digitale-vertrouwensinfrastructuur/calamity-service/utility/restclient"
)

type Handler struct {
	repository            calamity.Repository
	httpClient            restclient.HTTPClient
	validate              *validator.Validate
	policyServiceEndpoint string
}

func NewHandler(repo calamity.Repository, client restclient.HTTPClient, policyServiceEndpoint string) *Handler {
	return &Handler{
		repository:            repo,
		httpClient:            client,
		validate:              validator.New(),
		policyServiceEndpoint: policyServiceEndpoint,
	}
}

// Get a calamity by its address
// @Summary Get a Calamity
// @Description Get a calamity by its address.
// @Accept json
// @Produce json
// @Success 200 {object} models.Calamity
// @Router /api/v1/calamity/ [get]
func (h *Handler) Get(w http.ResponseWriter, r *http.Request) {
	address := r.FormValue("address")
	if address == "" {
		log.Println("Address provided is empty")
		respond.Error(w, http.StatusBadRequest, nil)
		return
	}

	policies, err := fetchPolicies(h.httpClient, h.policyServiceEndpoint)
	if err != nil {
		errString := fmt.Sprintf("Error occurred while trying to fetch policy on address '%s': %s", address, err.Error())
		log.Println(errString)
		respond.Error(w, http.StatusInternalServerError, errString)
		return
	}

	valid := containsValidPolicyOfType(policies, "read:calamities")
	if valid {
		//TODO: address should be provided to read calamity
		c, err := readCalamity(h.repository)
		if err != nil {
			errString := "Error occurred while trying to read calamity: " + err.Error()
			log.Println(errString)
			respond.Error(w, http.StatusInternalServerError, errString)
			return
		}
		respond.Render(w, http.StatusOK, *c)
	} else {
		respond.Error(w, http.StatusForbidden, "No valid policy found")
		return
	}
}

func containsValidPolicyOfType(policies *[]policy.Policy, policyType string) bool {
	for _, policy := range *policies {
		valid := isValidPolicyOfType(policy, policyType)
		if valid {
			return true
		}
	}
	return false
}

func fetchPolicies(client restclient.HTTPClient, policyServiceEndpoint string) (*[]policy.Policy, error) {
	log.Printf("Fetching policies on: %s", policyServiceEndpoint)
	response, err := client.Get(policyServiceEndpoint)
	if err != nil {
		log.Println("Error occurred while fetching policy")
		return nil, err
	}

	defer response.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(response.Body)

	var p []policy.Policy
	err = json.Unmarshal(bodyBytes, &p)
	if err != nil {
		log.Println("Error occurred while unmarshalling policy response")
		return nil, err
	}

	return &p, nil
}

func isValidPolicyOfType(policy policy.Policy, policyType string) bool {
	//TODO: policy should be properly typed, so we can use that type here
	if policy.Action.Scope == policyType {
		valid := isValidCalamityPolicy(policy)
		if valid {
			return true
		} else {
			fmt.Println("Invalid policy of type " + policyType)
			fmt.Printf("%+v\n", policy)
		}
	}
	return false
}

func isValidCalamityPolicy(policy policy.Policy) bool {
	return (policy.Actor == "digitale-huis" &&
		policy.Action.Scope == "read:calamities" &&
		policy.Actee.Actor == "calamiteitenregister" &&
		areAllConditionsValid(policy.Conditions))
}

func areAllConditionsValid(conditions []policy.Condition) bool {
	for _, condition := range conditions {
		if !isValidCondition(condition) {
			fmt.Println("Condition invalid: " + condition.Type)
			return false
		}
	}
	return true
}

func isValidCondition(condition policy.Condition) bool {
	if condition.Type == "POLICY_NOT_EXPIRED" {
		return true //TODO: implement expirationDate on policy
	} else if condition.Type == "CALAMITY_AT_ADDRESS" && condition.Authority.Actor == "meldkamer" {
		return true //TODO: implement CALAMITY CHECK
	}
	return false
}

func readCalamity(repo calamity.Repository) (*calamity.Calamity, error) {
	//TODO: get calamity from db
	// repo.All()
	return &calamity.Calamity{
		CalamityID: 42,
		Address:    "Duckstad, Kweklaan 36",
		Active:     true,
	}, nil
}
