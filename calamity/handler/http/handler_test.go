// Licensed under the MIT license

package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	chi "github.com/go-chi/chi/v5"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/digitale-vertrouwensinfrastructuur/calamity-service/calamity"
	"gitlab.com/digitale-vertrouwensinfrastructuur/calamity-service/policy"
	"gitlab.com/digitale-vertrouwensinfrastructuur/calamity-service/utility/mock"
)

//go:generate mockgen -package mock -source ../../handler.go -destination=../../mock/mock_handler.go

func TestHandler_Get(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	address := "Duckstad, Kweklaan 36"
	clientRespBody := []policy.Policy{{
		Actor: "digitale-huis",
		Action: policy.Action{
			Scope:       "read:calamities",
			Description: "bewonersinformatie lezen",
		},
		Actee: policy.Datasource{
			Actor: "calamiteitenregister",
			Url:   "http://localhost:8081/home/x",
		},
		Conditions: []policy.Condition{
			{
				Type: "POLICY_NOT_EXPIRED",
				Authority: policy.Datasource{
					Actor: "any-authority",
					Url:   "http://localhost:8088/some",
				},
				Description: "dat de toestemming niet verlopen is",
			},
		},
		Goal: "To test this",
	}}
	clientRespJson, err := json.Marshal(clientRespBody)
	assert.NoError(t, err)
	clientResponseBody := ioutil.NopCloser(bytes.NewReader(clientRespJson))
	clientResponse := http.Response{
		StatusCode: 200,
		Body:       clientResponseBody,
	}

	client := mock.NewMockHTTPClient(ctrl)
	clientRequestUrl := "http://localhost:3085/api/v1/policies"
	client.EXPECT().Get(clientRequestUrl).Return(&clientResponse, nil).AnyTimes()

	router := chi.NewRouter()
	RegisterHTTPEndPoints(router, nil, client, "http://localhost:3085/api/v1/policies")

	res := httptest.NewRecorder()
	req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("/api/v1/calamity/?address=%s", address), nil)
	assert.NoError(t, err)

	router.ServeHTTP(res, req)
	body, err := ioutil.ReadAll(res.Result().Body)
	assert.NoError(t, err)

	var gotCalamity calamity.CalamityResponse
	err = json.Unmarshal(body, &gotCalamity)
	assert.NoError(t, err)

	respBody := calamity.Calamity{
		CalamityID: 42,
		Address:    address,
		Active:     true,
	}

	assert.Equal(t, http.StatusOK, res.Code)
	assert.Equal(t, respBody.CalamityID, gotCalamity.CalamityID)
	assert.Equal(t, respBody.Address, gotCalamity.Address)
	assert.Equal(t, respBody.Active, gotCalamity.Active)
}
