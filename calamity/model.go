// Licensed under the MIT license

package calamity

import (
	"github.com/jinzhu/copier"

	"gitlab.com/digitale-vertrouwensinfrastructuur/calamity-service/utility/filter"
)

type CalamityRequest struct {
	Calamity
	filter.Filter
}

type CalamityResponse struct {
	Calamity
}

type Calamity struct {
	CalamityID int64  `json:"calamity_id"`
	Address    string `json:"occupants" validate:"required"`
	Active     bool   `json:"active" validate:"required"`
}

func NewCalamityResponse(calamity *Calamity) (CalamityResponse, error) {
	var response CalamityResponse

	err := copier.Copy(&response, &calamity)
	if err != nil {
		return response, err
	}

	return response, nil
}

func NewCalamitiesResponse(calamities []*Calamity) ([]CalamityResponse, error) {
	var responses []CalamityResponse

	for _, c := range calamities {
		var response CalamityResponse

		err := copier.Copy(&response, &c)
		if err != nil {
			return responses, err
		}
		responses = append(responses, response)
	}

	return responses, nil
}
