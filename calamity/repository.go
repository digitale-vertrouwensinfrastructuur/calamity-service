// Licensed under the MIT license

package calamity

import (
	"context"
)

type Repository interface {
	Create(ctx context.Context, oi *Calamity) (*Calamity, error)
	All(ctx context.Context, f *Filter) ([]*Calamity, error)
	Read(ctx context.Context, calamityUD int64) (*Calamity, error)
	Update(ctx context.Context, oi *Calamity) error
	Delete(ctx context.Context, calamityID int64) error
	Search(ctx context.Context, req *Filter) ([]*Calamity, error)
}
