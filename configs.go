// Licensed under the MIT license

package configs

import (
	"log"
	"os"
	"path/filepath"
	"time"

	arg "github.com/alexflint/go-arg"
)

var Args Configs

type Configs struct {
	Api
	Services
	FileServer
}

type Api struct {
	Name              string        `arg:"env:API_NAME" default:"calamity"`
	Host              string        `arg:"env:API_HOST" default:"0.0.0.0"`
	Port              string        `arg:"env:API_PORT" default:"3333"`
	Version           string        `arg:"env:API_VERSION" default:"v0.1.0"`
	ReadTimeout       time.Duration `arg:"env:API_READ_TIMEOUT" default:"5s"`
	ReadHeaderTimeout time.Duration `arg:"env:API_READ_HEADER_TIMEOUT" default:"5s"`
	WriteTimeout      time.Duration `arg:"env:API_WRITE_TIMEOUT" default:"10s"`
	IdleTimeout       time.Duration `arg:"env:API_IDLE_TIMEOUT" default:"120s"`
	RequestLog        bool          `arg:"env:API_REQUEST_LOG" default:"true"`
}

type Services struct {
	HttpClientTimeout     time.Duration `arg:"env:SERVICES_HTTP_CLIENT_TIMEOUT" default:"30s"`
	PolicyServiceEndpoint string        `arg:"env:SERVICES_POLICY_SERVICE_ENDPOINT" default:"http://localhost:3085/api/v1/policies"`
}

type FileServer struct {
	UiDir string `arg:"env:FILE_SERVER_UI_DIR"`
}

func (Configs) Version() string {
	return "Version: 1.0.0"
}

func New() *Configs {
	arg.MustParse(&Args)

	if Args.FileServer.UiDir == "" {
		workDir, _ := os.Getwd()
		Args.FileServer.UiDir = filepath.Join(workDir, "../../ui")
	}

	log.Printf("Config set to: %+v", Args)

	return &Args
}
