// Licensed under the MIT license

module gitlab.com/digitale-vertrouwensinfrastructuur/calamity-service

go 1.16

require (
	github.com/alexflint/go-arg v1.4.1
	github.com/go-chi/chi/v5 v5.0.1
	github.com/go-chi/cors v1.1.1
	github.com/go-playground/validator/v10 v10.4.1
	github.com/golang/mock v1.5.0
	github.com/jinzhu/copier v0.2.8
	github.com/jmoiron/sqlx v1.3.1
	github.com/stretchr/testify v1.7.0
)
