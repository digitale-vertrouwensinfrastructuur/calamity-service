version: '3'

dotenv: ['.env']

tasks:
  list:
    desc: Lists available commands
    cmds:
      - task -l

  run:
    desc: Runs the app
    cmds:
      - go run cmd/calamity-service/main.go

  check:
    desc: Checks integrity of program
    cmds:
      - task tidy
      - task vet
      - task golint
    #   - task security
      - task fmt
      - task test
    silent: true

  build:
    desc: Creates a compiled binary and copy configuration files
    cmds:
      - task: check
      - go mod download && CGO_ENABLED=0 GOOS=linux && go build -v -i -o calamity-service cmd/calamityservice/main.go
      - mkdir -p bin
      - mv calamity-service bin/

  tidy:
    desc: Downloads dependencies and removes unused ones
    cmds:
      - go mod tidy

  vet:
    desc: Vets code
    cmds:
      - go vet ./...

  golint:
    desc: Uses golangci-lint
    cmds:
      - golangci-lint run

  security:
    desc: Run golang security
    cmds:
      - gosec -quiet ./...

  fmt:
    desc: Lint code
    cmds:
      - go fmt ./...

  test:
    desc: Test all code
    cmds:
      - go test ./...

  coverage:
    desc: Perform test coverage
    cmds:
      - go test -cover ./...

  generate:
    desc: Runs all //go:generate commands embedded in .go files
    cmds:
      - go generate ./...

  race:
    desc: Check race condition
    cmds:
      - go test -race ./...

  swagger:
    desc: Generates Swagger page for API reference
    cmds:
      - swag init -g cmd/go8/main.go

  docker-build:
    desc: Builds a Docker image a server container
    cmds:
      - docker build -t calamity-service/server -f Dockerfile .

  docker-run:
    desc: Runs the app Docker image as a Docker container
    cmds:
      - docker run -p 3333:3333 --rm -it --net=host --name calamity_service_container calamity-service/server

  docker-compose-start:
    desc: Runs server and database using docker-compose
    cmds:
      - docker-compose -f docker-compose.yml up --build --abort-on-container-exit

  docker-compose-stop:
    desc: Stops server and database using docker-compose
    cmds:
      - docker-compose -f docker-compose.yml down --volumes

  install-tools:
    desc: Install all optional cli tools
    cmds:
      - task: install-golangci
      - task: install-swagger
      - task: install-testify
      - task: install-gomock
    #   - task: install-golangmigrate
    #   - task: install-sqlboiler
      - task: install-gosec
    silent: true

#   install-sqlboiler:
#     desc: Install sqlboiler orm
#     cmds:
#       - GO111MODULE=off go get -u -t github.com/volatiletech/sqlboiler
#       - GO111MODULE=off go get github.com/volatiletech/sqlboiler/drivers/sqlboiler-psql

  install-golangci:
    desc: Install golangci linter
    cmds:
      - curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin

  install-swagger:
    desc: Install swagger cli
    cmds:
      - go get github.com/swaggo/swag/cmd/swag

  install-testify:
    desc: Install testify testing suite
    cmds:
      - go get github.com/stretchr/testify

  install-gomock:
    desc: Install gomock test framework
    cmds:
      - GO111MODULE=on go get github.com/golang/mock/mockgen@v1.4.4

#   install-golangmigrate:
#     desc: Install golang migration tool
#     cmds:
#       - curl -L https://github.com/golang-migrate/migrate/releases/download/v4.14.1/migrate.linux-amd64.tar.gz | tar xvz
#       - mkdir -p ~/.local/bin
#       - mv migrate.linux-amd64 ~/.local/bin/migrate

  install-gosec:
    desc: Install golang security check. https://github.com/securego/gosec
    cmds:
      - go get github.com/securego/gosec/v2/cmd/gosec