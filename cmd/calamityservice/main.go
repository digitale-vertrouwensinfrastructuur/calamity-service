// Licensed under the MIT license

package main

import (
	"log"

	"gitlab.com/digitale-vertrouwensinfrastructuur/calamity-service/server"
)

func main() {
	s := server.New()
	s.Init()

	if err := s.Run(); err != nil {
		log.Fatalf("%s", err.Error())
	}
}
