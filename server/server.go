// Licensed under the MIT license

package server

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"

	"github.com/go-chi/chi/v5"
	chiMiddleware "github.com/go-chi/chi/v5/middleware"

	"github.com/jmoiron/sqlx"
	configs "gitlab.com/digitale-vertrouwensinfrastructuur/calamity-service"
	"gitlab.com/digitale-vertrouwensinfrastructuur/calamity-service/middleware"
)

type Server struct {
	cfg        *configs.Configs
	db         *sqlx.DB
	router     *chi.Mux
	httpServer *http.Server
}

func New() *Server {
	log.Println("Starting API")
	return &Server{}
}

func (s *Server) Init() {
	s.newConfigs()
	s.newRouter()
	s.newFileServer()
	s.initHealth()
	s.initCalamity()
}

func (s *Server) newConfigs() {
	s.cfg = configs.New()
}

func (s *Server) newRouter() {
	s.router = chi.NewRouter()
	s.router.Use(middleware.Cors)
	if s.cfg.Api.RequestLog {
		s.router.Use(chiMiddleware.Logger)
	}
	s.router.Use(chiMiddleware.Recoverer)
}

func (s *Server) newFileServer() {
	filesDir := http.Dir(s.cfg.FileServer.UiDir)
	FileServer(s.router, "/ui", filesDir)
}

// FileServer conveniently sets up a http.FileServer handler to serve
// static files from a http.FileSystem.
func FileServer(r chi.Router, path string, root http.FileSystem) {
	if strings.ContainsAny(path, "{}*") {
		panic("FileServer does not permit any URL parameters.")
	}

	if path != "/" && path[len(path)-1] != '/' {
		r.Get(path, http.RedirectHandler(path+"/", http.StatusMovedPermanently).ServeHTTP)
		path += "/"
	}
	path += "*"

	r.Get(path, func(w http.ResponseWriter, r *http.Request) {
		rctx := chi.RouteContext(r.Context())
		pathPrefix := strings.TrimSuffix(rctx.RoutePattern(), "/*")
		fs := http.StripPrefix(pathPrefix, http.FileServer(root))
		fs.ServeHTTP(w, r)
	})
}

func (s *Server) Run() error {
	s.httpServer = &http.Server{
		Addr:           ":" + s.cfg.Api.Port,
		Handler:        s.router,
		ReadTimeout:    s.cfg.Api.ReadTimeout,
		WriteTimeout:   s.cfg.Api.WriteTimeout,
		MaxHeaderBytes: 1 << 20,
	}

	go func() {
		printAllRegisteredRoutes(s.router)
		err := s.httpServer.ListenAndServe()
		if err != nil {
			log.Fatal(err)
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)

	<-quit

	ctx, shutdown := context.WithTimeout(context.Background(), s.cfg.Api.IdleTimeout)
	defer shutdown()

	return s.httpServer.Shutdown(ctx)
}

func (s *Server) GetConfig() *configs.Configs {
	return s.cfg
}

func (s *Server) GetDB() *sqlx.DB {
	return s.db
}

func printAllRegisteredRoutes(router *chi.Mux) {
	walkFunc := func(method string, path string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) error {
		log.Printf("path: %s method: %s ", path, method)
		return nil
	}
	if err := chi.Walk(router, walkFunc); err != nil {
		log.Print(err)
	}
}
