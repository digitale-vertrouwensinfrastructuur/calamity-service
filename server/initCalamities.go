// Licensed under the MIT license

package server

import (
	"net/http"

	calamityHandler "gitlab.com/digitale-vertrouwensinfrastructuur/calamity-service/calamity/handler/http"
	calamityRepo "gitlab.com/digitale-vertrouwensinfrastructuur/calamity-service/calamity/repository/inmemory"
)

func (s *Server) initCalamity() {
	s.initCalamities()
}

func (s *Server) initCalamities() {
	client := &http.Client{
		Timeout: s.cfg.Services.HttpClientTimeout,
	}

	newCalamityRepo := calamityRepo.New(s.GetDB())

	calamityHandler.RegisterHTTPEndPoints(s.router, newCalamityRepo, client, s.cfg.Services.PolicyServiceEndpoint)
}
