// Licensed under the MIT license

package server

import (
	"net/http"

	"github.com/go-chi/chi/v5"
)

func (s *Server) initHealth() {
	s.router.Route("/health", func(router chi.Router) {
		router.Get("/", GetHealth)
	})
}

func GetHealth(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte("healthy"))
}
