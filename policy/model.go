// Licensed under the MIT license

package policy

import (
	"github.com/jinzhu/copier"

	"gitlab.com/digitale-vertrouwensinfrastructuur/calamity-service/utility/filter"
)

type PolicyRequest struct {
	Policy
	filter.Filter
}

type PolicyResponse struct {
	Policy
}

type Policy struct {
	PolicyID   int64       `json:"policy_id"`
	Actor      string      `json:"actor" validate:"required"`
	Action     Action      `json:"action" validate:"required"`
	Actee      Datasource  `json:"actee" validate:"required"`
	Conditions []Condition `json:"conditions" validate:"required"`
	Goal       string      `json:"goal" validate:"required"`
}

type Action struct {
	Scope       string `json:"scope" validate:"required"`
	Description string `json:"description" validate:"required"`
}

type Datasource struct {
	Actor string `json:"actor" validate:"required"`
	Url   string `json:"url" validate:"required"`
}

type Condition struct {
	Type        string     `json:"type" validate:"required"`
	Authority   Datasource `json:"authority" validate:"required"`
	Description string     `json:"description" validate:"required"`
}

func NewPolicyResponse(policy *Policy) (PolicyResponse, error) {
	var response PolicyResponse

	err := copier.Copy(&response, &policy)
	if err != nil {
		return response, err
	}

	return response, nil
}

func NewPoliciesResponse(policies []*Policy) ([]PolicyResponse, error) {
	var responses []PolicyResponse

	for _, p := range policies {
		var response PolicyResponse

		err := copier.Copy(&response, &p)
		if err != nil {
			return responses, err
		}
		responses = append(responses, response)
	}

	return responses, nil
}
